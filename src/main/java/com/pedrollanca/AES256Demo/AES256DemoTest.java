package com.pedrollanca.AES256Demo;

import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;

import junit.framework.TestCase;

public class AES256DemoTest extends TestCase {
	AES256Demo aes;
	String message="Plain message";
	
	protected void setUp() throws Exception {
		aes=new AES256Demo("8AB58079D08F6ECEA45EBC1C9EECBC013D5D4308D3648A0158702AEAC2B3549E", "E63F91DD44D329F282816747F3ED72E6");
		super.setUp();
	}

	public void testAES256Demo() {
		assertNotNull(aes);
	}

	public void testEncrypt() throws DataLengthException, IllegalStateException, InvalidCipherTextException {
		String enc=aes.encrypt(message);
		System.out.println(enc);
		assertNotNull(enc);
	}

	public void testDecrypt() throws DataLengthException, IllegalStateException, InvalidCipherTextException {
		String enc=aes.encrypt(message);
		String dec=aes.decrypt(enc);
		System.out.println(dec);
		assertNotNull(dec);
	}

}
