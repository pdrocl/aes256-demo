package com.pedrollanca.AES256Demo;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.digests.SHA3Digest;
import org.bouncycastle.crypto.engines.RijndaelEngine;
import org.bouncycastle.crypto.generators.PKCS12ParametersGenerator;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.encoders.Base64;

public class AES256Demo {
		private static char[] _key;
	    private static byte[] _iv;
	    
	    /** Demo class for encryption and decryption using the AES256 (Rijndael) algorithm with the Bouncy Castle provider. 
	     * @param key
	     * @param iv
	     */
	    public AES256Demo(String key, String iv){
	    	_key=key.toCharArray();
	    	_iv=iv.getBytes();
	    }
	    
	    /**
	     * @param inputText
	     * @return
	     * @throws DataLengthException
	     * @throws IllegalStateException
	     * @throws InvalidCipherTextException
	     */
	    public String encrypt(String inputText) throws DataLengthException, IllegalStateException, InvalidCipherTextException {
	    	int iterationCount = 5;
	    	
	        PKCS12ParametersGenerator pGen = new PKCS12ParametersGenerator(new SHA3Digest(256));
	        char[] passwordChars = _key;

	        final byte[] pkcs12PasswordBytes = PBEParametersGenerator.PKCS12PasswordToBytes(passwordChars);
	        pGen.init(pkcs12PasswordBytes, _iv , iterationCount );     

	        BlockCipher engine = new RijndaelEngine(256);
	        CBCBlockCipher cbc = new CBCBlockCipher(engine);
	        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(cbc, new PKCS7Padding());

	        ParametersWithIV aesCBCParams = (ParametersWithIV) pGen.generateDerivedParameters(256, 256);
	        cipher.init(true, aesCBCParams);

	        byte[] input = inputText.getBytes();
	        byte[] cipherText = new byte[cipher.getOutputSize(input.length)];

	        int cipherLength = cipher.processBytes(input, 0, input.length, cipherText, 0);
	        cipher.doFinal(cipherText, cipherLength);

	        String result = new String(Base64.encode(cipherText));
	        return result;
	      
	    }

	    /**
	     * @param inputText
	     * @return
	     * @throws DataLengthException
	     * @throws IllegalStateException
	     * @throws InvalidCipherTextException
	     */
	    public String decrypt(String inputText) throws DataLengthException, IllegalStateException, InvalidCipherTextException {
	    	int iterationCount = 5;
	    	
	    	PKCS12ParametersGenerator pGen = new PKCS12ParametersGenerator(new SHA3Digest(256));
	        char[] passwordChars = _key;

	        final byte[] pkcs12PasswordBytes = PBEParametersGenerator.PKCS12PasswordToBytes(passwordChars);
	        pGen.init(pkcs12PasswordBytes, _iv , iterationCount );     

	        BlockCipher engine = new RijndaelEngine(256);
	        CBCBlockCipher cbc = new CBCBlockCipher(engine);
	        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(cbc, new PKCS7Padding());

	        ParametersWithIV aesCBCParams = (ParametersWithIV) pGen.generateDerivedParameters(256, 256);
	        cipher.init(false, aesCBCParams);

	        byte[] output = Base64.decode(inputText.getBytes());
	        byte[] cipherText = new byte[cipher.getOutputSize(output.length)];

	        int cipherLength = cipher.processBytes(output, 0, output.length, cipherText, 0);
	        int outputLength = cipher.doFinal(cipherText, cipherLength);
	        outputLength += cipherLength;

	        byte[] resultBytes = cipherText;
	        if (outputLength != output.length) {
	            resultBytes = new byte[outputLength];
	            System.arraycopy(
	                    cipherText, 0,
	                    resultBytes, 0,
	                    outputLength
	                    );
	        }

	        String result = new String(resultBytes);
	        return result;
	    }
}
